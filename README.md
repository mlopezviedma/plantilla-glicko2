# Plantilla Glicko2

Plantilla de proyecto para generar Sistemas de Graduación usando Glicko2.

Ejecute `./update --help` para obtener ayuda acerca de cómo usar el proyecto.
Antes de agregar un commit inicial, haga lo siguiente:

1. Vacíe el fichero `README.md`. Si lo desea, puede documentar su proyecto
mediante un archivo llamado `DOC.md`; el script `update` generará, en cada
commit, el archivo `README.md` a partir de él y de las puntuaciones actuales.

2. Defina las variables que se encuentran al comienzo del script `update`:
 - `readme_title`: Este será el título de su archivo `README.md`.
 - `ratings_table_title`: En el archivo `README.md`, este será el título de la
tabla de puntuaciones.
 - `logfile_name`: Nombre de los archivos de registro que serán generados en
cada periodo Glicko.
 - `update_message`: Mensaje de commit por defecto que será mostrado en cada
periodo Glicko.
 - `remote`: Nombre del repositorio remoto (si no existe un
repositorio remoto, simplemente deje esta variable vacía).
 - `default_rank`: El grado por defecto que se asignará a los jugadores al
ser creados o reasignados.
 - `anchor_sd`: Desviación estándar asignada para grados ancla.

