#include "gobbrankingsystem.h"

void GoBBRankingSystem() { }

void GoBBRankingSystem::loadPlayersFile(string path)
{
	fstream file;
	string line, name, comments;
	int i, j;
	double rating, rd, volatility;
	Glicko2Player *player;
	file.open(path);
	getline(file, line); // First line is ignored
	while (getline(file, line))
	{
		i = 0;
		rating = 1500;
		rd = 350;
		volatility = 0.06;
		comments = "";
		while ( i < line.length() && (line[i] != '\t') ) ++i; // First field is ignored
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		name = line.substr(j, i-j);
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i; // Third field is ignored
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		if (i > j) rating = stod(line.substr(j, i-j));
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		if (i > j) rd = stod(line.substr(j, i-j));
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		if (i > j) volatility = stod(line.substr(j, i-j));
		j = ++i;
		if (j < line.length()) comments = line.substr(j, line.length()-j);
		player = new Glicko2Player(name, rating, rd, volatility, comments);
		cout << "Player #" << Glicko2Player::getPlayersCount() << ": " << name << " ("
			<< rating << "±2*" << rd << ")/" << volatility << " ─ " << comments << endl;
	}
	file.close();
}

void GoBBRankingSystem::loadMatchesFile(string path)
{
	fstream file;
	string line;
	int i, j, id1, id2, ret;
	double result;
	file.open(path);
	getline(file, line); // First line is ignored
	while (getline(file, line))
	{
		i = 0;
		while ( i < line.length() && (line[i] != '\t') ) ++i; // First field is ignored
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		if (i > j) id1 = stoi(line.substr(j, i-j)); else continue;
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		if (i > j) id2 = stoi(line.substr(j, i-j)); else continue;
		j = ++i;
		while ( i < line.length() && (line[i] != '\t') ) ++i;
		if (i > j) {
			if (line.substr(j, i-j) == "0") result = 0;
			else if (line.substr(j, i-j) == "1") result = 1;
			else if (line.substr(j, i-j) == "2") result = .5;
			else result = -1;
		}
		else continue;
		ret = Glicko2Player::addMatch(id1, id2, result);
		cout << "Player #" << id1;
		if (result == 1) cout << " won against Player #";
		else if (result == 0) cout << " lost against Player #";
		else if (result == .5) cout << " drew with Player #";
		else cout << " vs. Player #";
		cout << id2 << " ─ ";
		if (ret == 0) cout << "Reported" << endl;
		else if (ret == 1) cout << "Aborted (players are the same)" << endl;
		else if (ret == 2) cout << "Aborted (non-existent player/s)" << endl;
		else if (ret == 3) cout << "Aborted (game result is not valid)" << endl;
	}
	file.close();
}

void GoBBRankingSystem::writePlayersFile(string path)
{
	ofstream file;
	list<Glicko2Player*>::iterator p;
	list<Glicko2Player*> players = Glicko2Player::getPlayers();
	file.open(path);
	file << "ID" << '\t' << "Nombre" << '\t' << "Grado" << '\t' << "Puntuación"
		<< '\t' << "Desviación" << '\t' << "Volatilidad" << '\t' << "Comentarios" << endl;
	for (p = players.begin(); p != players.end(); ++p)
		file << (*p)->getID() << '\t' << (*p)->getName() << '\t' << rank(*p) << '\t'
			<< fixed << setprecision(3) << (*p)->getRating() << '\t' << (*p)->getRD() << '\t'
			<< setprecision(5) << (*p)->getVolatility() << '\t' << (*p)->getComments() << endl;
	file.close();	
}

string GoBBRankingSystem::rank(Glicko2Player *player)
{
	stringstream ss;
	int rank;
	double rating = player->getRating();
	if (rating < 2100)
	{
		if (rating < 200) rank = 20;
		else rank = 21 - ((int) rating) / 100;
		ss << rank << 'k';
	}
	else {
		if (rating >= 2900) rank = 9;
		else rank = ((int) rating) / 100 - 20;
		ss << rank << 'd';
	}
	if (player->getRD() > 75) ss << '?';
	return ss.str();
}

