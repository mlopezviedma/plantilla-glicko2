﻿#ifndef GOBBRANKINGSYSTEM_H
#define GOBBRANKINGSYSTEM_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include "glicko2player.h"

using namespace std;

class GoBBRankingSystem
{
public:
	GoBBRankingSystem();
	static void loadPlayersFile(string path = "./players.tsv");
	static void loadMatchesFile(string path = "./matches.tsv");
	static void writePlayersFile(string path = "./updated.tsv");
	static string rank(Glicko2Player *player);
};

#endif // GOBBRANKINGSYSTEM_H
