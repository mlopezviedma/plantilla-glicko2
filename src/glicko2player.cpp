﻿#include "glicko2player.h"

double Glicko2Player::tau = .6;
int Glicko2Player::playersCount = 0;
int Glicko2Player::idsCount = 0;
list<Glicko2Player*> Glicko2Player::players;
list<t_glicko2match*> Glicko2Player::matches;

Glicko2Player::Glicko2Player(string name, double rating, double rd, double volatility, string comments)
{
	this->id = ++idsCount;
	this->name = name;
	this->rating = rating;
	this->rd = rd;
	this->sigma = volatility;
	this->mu = (this->rating - 1500)/173.7178;
	this->phi = this->rd/173.7178;
	this->new_mu = mu;
	this->new_phi = phi;
	this->comments = comments;
	players.push_back(this);
	++playersCount;
}

Glicko2Player::~Glicko2Player()
{
	list<Glicko2Player*>::iterator p;
	for (p = players.begin(); p != players.end(); ++p)
		if ((*p)->id == this->id) break;
	players.erase(p);
	--playersCount;
}

int Glicko2Player::addMatch(t_glicko2match match)
{
	if (match.id1 == match.id2) return 1;
	list<Glicko2Player*>::iterator p;
	bool id1exists, id2exists;
	for (p = players.begin(); p != players.end(); ++p)
	{
		if ((*p)->id == match.id1) id1exists = true;
		if ((*p)->id == match.id2) id2exists = true;
		if (id1exists && id2exists) break;
	}
	if ( !(id1exists && id2exists) ) return 2;
	if ( (match.result < 0) || (match.result > 1) ) return 3;
	t_glicko2match* m = new t_glicko2match;
	m->id1 = match.id1;
	m->id2 = match.id2;
	m->result = match.result;
	matches.push_back(m);
	return 0;
}

int Glicko2Player::addMatch(int id1, int id2, double result)
{
	t_glicko2match match;
	match.id1 = id1;
	match.id2 = id2;
	match.result = result;
	return addMatch(match);
}

int Glicko2Player::addMatch(int id1, int id2, bool draw)
{
	t_glicko2match match;
	match.id1 = id1;
	match.id2 = id2;
	if (draw) match.result = 0.5; else match.result = 1;
	return addMatch(match);
}

void Glicko2Player::update()
{
	list<Glicko2Player*>::iterator p, q;
	list<t_glicko2match*>::iterator m;
	list<t_glicko2match*> my_matches;
	t_glicko2match* match;
	Glicko2Player* opponent;
	double result, v, delta, mu_sum, E, g, a, x, x_prev, x_temp, d1, d2, h1, h2,
		expx, tau2, phi2, delta2;
	bool played, match_found;
	tau2 = tau*tau;
	for (p = players.begin(); p != players.end(); ++p)
	{
		v = 0;
		delta = 0;
		mu_sum = 0;
		phi2 = (*p)->phi*(*p)->phi;
		played = false;
		for (m = matches.begin(); m != matches.end(); ++m)
		{
			match_found = false;
			if ((*p)->id == (*m)->id1)
			{
				match = new t_glicko2match;
				match->id1 = (*p)->id;
				match->id2 = (*m)->id2;
				match->result = (*m)->result;
				my_matches.push_back(match);
				match_found = true;
			}
			else if ((*p)->id == (*m)->id2)
			{
				match = new t_glicko2match;
				match->id1 = (*p)->id;
				match->id2 = (*m)->id1;
				match->result = 1 - (*m)->result;
				my_matches.push_back(match);
				match_found = true;
			}
			if (match_found)
			{
				for (q = players.begin(); q != players.end(); ++q)
					if ((*q)->id == match->id2) break;
				if (q != players.end())
				{
					E = E_of((*p)->mu, (*q)->mu, (*q)->phi);
					g = g_of((*q)->phi);
					v += g*g*E*(1-E);
					delta += g*(match->result-E);
					played = true;
				}
			}
		}
		if (played)
		{
			v = 1.0/v;
			mu_sum = delta;
			delta = v*delta;
			a = log((*p)->sigma*(*p)->sigma);
			x_prev = a;
			x = x_prev;
			delta2 = delta*delta;
			do
			{
				expx = exp(x_prev);
				d1 = phi2 + v + expx;
				d2 = delta2/d1/d1;
				h1 = -(x_prev - a)/tau2 - 0.5*expx/d1 + 0.5*expx*d2;
				h2 = -1.0/tau2 - 0.5*expx*(phi2 + v)/d1/d1 + 0.5*delta2*expx*(phi2 + v - expx)/d1/d1/d1;
				x_temp = x;
				x = x_prev - h1/h2;
				x_prev = x_temp;
			} while (abs(x - x_prev) > 0.00001);
			(*p)->sigma = exp(x/2);
		}
		(*p)->new_phi = sqrt(phi2 + (*p)->sigma*(*p)->sigma);
		if (played)
		{
			(*p)->new_phi = 1.0/sqrt(1.0/(*p)->new_phi/(*p)->new_phi + 1.0/v);
			(*p)->new_mu += (*p)->new_phi*(*p)->new_phi*mu_sum;
		}
		my_matches.clear();
	}
	for (p = players.begin(); p != players.end(); ++p)
	{
		(*p)->mu = (*p)->new_mu;
		(*p)->phi = (*p)->new_phi;
		(*p)->rating = 173.7178*(*p)->mu + 1500;
		(*p)->rd = 173.7178*(*p)->phi;
	}
	matches.clear();
}

