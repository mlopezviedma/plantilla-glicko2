﻿#ifndef GLICKO2PLAYER_H
#define GLICKO2PLAYER_H

#include <cmath>
#include <list>
#include <string>

using namespace std;

struct t_glicko2match // Match between two players in the list
{
	int id1, // ID of the first player
	    id2; // ID of the second player
	double result; // Match result
};

class Glicko2Player
{
public:
	Glicko2Player(string name, double rating = 1500, double rd = 350, double volatility = .06, string comments = "");
	~Glicko2Player();
	static void setTau(double new_tau) { new_tau > 0 ? tau = new_tau : tau = 0; }
	static double getTau() { return tau; }
	static list<Glicko2Player*> getPlayers() { return players; }
	static int getPlayersCount() { return playersCount; }
	int getID() const { return id; }
	string getName() const { return name; }
	double getRating() const { return rating; }
	double getRD() const { return rd; }
	double getVolatility() const { return sigma; }
	string getComments() const { return comments; }
	static int addMatch(t_glicko2match match);
	static int addMatch(int id1, int id2, double result);
	static int addMatch(int id1, int id2, bool draw);
	static void update();

private:
	static int playersCount; // Number of players that are in the list now
	static int idsCount; // IDs of the players must be unique
	static double tau; // Volatility time constant
	static list<Glicko2Player*> players; // List of players
	static list<t_glicko2match*> matches; // List of non-updated matches
	static double g_of(double phi) { return 1.0 / sqrt(1.0 + 3.0*phi*phi/M_PI/M_PI); }
	static double E_of(double mu, double mu_j, double phi_j) { return 1.0 / (1.0 + exp(-g_of(phi_j)*(mu - mu_j))); }
	int id; // ID of the player
	string name, comments; // Name and comments for the player
	double rating, rd, sigma, mu, phi, new_mu, new_phi; // Rating parameters of the player
};

#endif // GLICKO2PLAYER_H
