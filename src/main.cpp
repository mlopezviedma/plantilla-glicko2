﻿#include "gobbrankingsystem.h"

int main(int argc, char *argv[])
{
	GoBBRankingSystem::loadPlayersFile();
	GoBBRankingSystem::loadMatchesFile();
	Glicko2Player::update();
	GoBBRankingSystem::writePlayersFile();
	return 0;
}

