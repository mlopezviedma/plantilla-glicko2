#!/bin/bash

readme_title=""
ratings_table_title="Puntuaciones actuales"
logfile_name="$(date +%Y-%m-%d)"
update_message="Periodo Glicko $(date +%B\ %Y)"
remote=origin
default_rank=14k
anchor_sd=25

trap "exit 1" TERM
export top_pid=$$

function exit_err ()
{
	echo "$0: $1" > /dev/stderr
	kill -s TERM $top_pid
}

function check_digit ()
{
	case $1 in ''|*[!0-9]*) exit_err "Argument «$1» must be a natural number";; esac
	return 0
}

function ask_confirmation ()
{
	local option
	echo -n "$1 y/n " >&2
	read -sn1 option
	echo >&2
	[ "$option" != "y" ] && [ "$option" != "Y" ] && exit_err "Aborted by user"
	return 0
}

function rank2glicko ()
{
	local n=${1::-1}
	local g=${1:(-1)}
	case $n in ''|*[!0-9]*) exit_err "Rank «$1» is not valid. Write for example «5k», «2d», etc.";; esac
	if [ "$g" == "k" ]; then
		if (( $n > 0 )) && (( $n < 21 )); then echo $(( 100*(21-n)+50 ))
		else exit_err "Valid Kyu ranks are between 1k and 20k."; fi
	elif [ "$g" == "d" ]; then
		if (( $n > 0 )) && (( $n < 10 )); then echo $(( 100*n+2050 ))
		else exit_err "Valid Dan ranks are between 1d and 9d."; fi
	else exit_err "Rank «$1» is not valid. Write for example «5k», «2d», etc."; fi
	return 0
}

usage="[ -hn ] [ MSG | -acdACDre ]"
players_file=players.tsv
matches_file=matches.tsv
updated_file=updated.tsv
doc_file=DOC.md
default_glicko="$(rank2glicko $default_rank)"
while :; do
	case $1 in
	--)
		shift
		break;;
	--*)
		case ${1:2} in
		help) help=1;;
		add)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=add
			if [ -z "$2" ]; then exit_err "An argument is expected for option '--add'"
			else add="$2"; fi
			if [ -n "$3" ]; then
				rank="$3"
				shift
			fi
			shift;;
		change)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=change
			if [ -z "$2" ]; then exit_err "An argument is expected for option '--change'"
			else id="$2"; fi
			if [ -n "$3" ]; then
				rank="$3"
				shift
			fi
			shift;;
		doc)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=doc;;
		anchor)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=anchor
			if [ -z "$3" ]; then exit_err "Two arguments are expected for option '--anchor'"
			else
				id="$2"
				rank="$3"
			fi
			shift 2;;
		comment)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=comment
			if [ -z "$2" ]; then exit_err "An argument is expected for option '--comment'"
			else id="$2"; fi
			if [ -n "$3" ]; then
				comments="$3"
				shift
			fi
			shift;;
		delete)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=delete
			if [ -z "$2" ]; then exit_err "An argument is expected for option '--delete'"
			else id="$2"; fi
			shift;;
		report)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=report
			if [ -z "$4" ]; then exit_err "Three arguments are expected for option '--report'"
			else
				black="$2"
				white="$3"
				result="$4"
			fi
			shift 3;;
		empty)
			[ -n "$action" ] && exit_err "Only one action can be specified"
			action=empty;;
		no-commit) no_commit=1;;
		*) exit_err "Option not recognized: ${1:2}";;
		esac;;
		-*)
			for i in $(seq 2 ${#1}); do
				case ${1:i-1:1} in
				h) help=1;;
				a)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=add
					if [ -z "$2" ]; then exit_err "An argument is expected for option '-a'"
					else add="$2"; fi
					if [ -n "$3" ]; then
						rank="$3"
						shift
					fi
					shift;;
				c)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=change
					if [ -z "$2" ]; then exit_err "An argument is expected for option '-c'"
					else id="$2"; fi
					if [ -n "$3" ]; then
						rank="$3"
						shift
					fi
					shift;;
				d)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=doc;;
				A)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=anchor
					if [ -z "$3" ]; then exit_err "Two arguments are expected for option '-A'"
					else
						id="$2"
						rank="$3"
					fi
					shift 2;;
				C)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=comment
					if [ -z "$2" ]; then exit_err "An argument is expected for option '-C'"
					else id="$2"; fi
					if [ -n "$3" ]; then
						comments="$3"
						shift
					fi
					shift;;
				D)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=delete
					if [ -z "$2" ]; then exit_err "An argument is expected for option '-D'"
					else id="$2"; fi
					shift;;
				r)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=report
					if [ -z "$4" ]; then exit_err "Three arguments are expected for option '-r'"
					else
						black="$2"
						white="$3"
						result="$4"
					fi
					shift 3;;
				e)
					[ -n "$action" ] && exit_err "Only one action can be specified"
					action=empty;;
				n) no_commit=1;;
				*) exit_err "Option not recognized: ${1:i-1:1}";;
				esac
			done;;
	*) break;;
	esac
	shift
done

if [ -n "$help" ]; then
	cat <<EOF
Usage: $0 $usage
Updates the ranking system. If no action is specified, a new Glicko period is
calculated by a way of adding the matches reported on the file «$matches_file»,
in which case MSG replaces the default commit description, if provided.
  -h, --help                Prints this help and exits.
  -a, --add NAME [ RANK ]   Adds a new player to the system. If RANK is
                            provided, it is assigned with a SD of 150.
                            Otherwise, $default_glicko is assigned with a SD of 350.
  -c, --change ID [ RANK ]  Changes the rank of player ID. If RANK is
                            provided, it is assigned with a SD of 150.
                            Otherwise, $default_glicko is assigned with a SD of 350.
  -d, --doc [ COMMIT_MSG ]  Only updates the files $doc_file and README.md. COMMIT_MSG
                            replaces the default commit message, if provided.
  -A, --anchor ID/NAME RANK  Changes the rank of player ID to RANK / Adds a new
                            player to the system as RANK. In both cases, a SD of
                            $anchor_sd will be given (so called "system anchor").
  -C, --comment ID [ COMMENT ]  Changes the comments field for player ID.
  -D, --delete ID           Deletes player ID from the system. There cannot be any
                            reported matches at that moment.
  -r, --report ID_BLACK ID_WHITE RESULT  Reports a game for the next period. RESULT
                            is as follows: 0: White wins; 1: Black wins; 2: Draw.
  -e, --empty               Removes all reported matches from «$matches_file».
  -n, --no-commit           Don't execute git commands (for testing purposes).
EOF
	exit 0;
fi

case $action in
	"")
		[ -n "$1" ] && update_message="$1"
		[ ! -f src/update-ratings ] && make -C src/
		src/update-ratings | tee --append "$logfile_name".log
		mv $updated_file $players_file
		if [ -z "$no_commit" ]; then
			git add $players_file $matches_file
			temp_file=$(mktemp)
			head -n 1 $matches_file > $temp_file
			mv $temp_file $matches_file
			echo "Reported matches removed"
		fi
		commit_msg="Actualización del sistema - $update_message";;
	add)
		id=$(cat $players_file | wc -l)
		if [ -z "$rank" ]; then
			rank="$default_rank"
			rating="$default_glicko"
			sd=350
		else
			rating=$(rank2glicko $rank)
			sd=150
		fi
		ask_confirmation "Add new player $add to the system with rank $rank and SD $sd?"
		echo "Player #${id}: $add (${rating}±2*${sd})/0.06"
		echo -e "$id\t$add\t${rank}?\t${rating}.000\t${sd}.000\t0.06000\t" >> $players_file
		[ -z "$no_commit" ] && git add $players_file
		commit_msg="Nuevo jugador $add agregado al sistema";;
	change)
		check_digit $id
		n=$(cat $players_file | wc -l)
		if (( id > 0 )) && (( id < n )); then
			if [ -z "$rank" ]; then
				rank="$default_rank"
				rating="$default_glicko"
				sd=350
			else
				rating=$(rank2glicko $rank)
				sd=150
			fi
			name="$(sed "$((id+1))q;d" $players_file | cut -f 2)"
			ask_confirmation "Change rank for player $name to $rank with SD $sd?"
			comments="$(sed "$((id+1))q;d" $players_file | cut -f 7)"
			echo "Player #${id}: $name (${rating}±2*${sd})/0.06"
			temp_file=$(mktemp)
			head -n $id $players_file > $temp_file
			echo -e "$id\t$name\t${rank}?\t${rating}.000\t${sd}.000\t0.06000\t$comments" >> $temp_file
			tail -n +$((id+2)) $players_file >> $temp_file
			mv $temp_file $players_file
			[ -z "$no_commit" ] && git add $players_file
			commit_msg="Cambio de grado para $name"
		else exit_err "Argument «$id» must be a valid player ID"; fi;;
	doc)
		if [ -n "$1" ]; then commit_msg="$1"; else commit_msg="Documentación actualizada"; fi
		[ -f $doc_file ] && [ -z "$no_commit" ] && git add $doc_file;;
	anchor)
		rating=$(rank2glicko $rank)
		sd=$anchor_sd
		case "$id" in
		''|*[!0-9]*) # $id is not a number, so it's considered as a new player's name
			ask_confirmation "Add new player $id to the system with $rank as an anchor rank?"
			add="$id"
			id=$(cat $players_file | wc -l)
			echo "Player #${id}: $add (${rating}±2*${sd})/0.06"
			echo -e "$id\t$add\t${rank}\t${rating}.000\t${sd}.000\t0.06000\t" >> $players_file
			[ -z "$no_commit" ] && git add $players_file
			commit_msg="Nuevo jugador $add ($rank) agregado al sistema";;
		*)
			n=$(cat $players_file | wc -l)
			if (( id > 0 )) && (( id < n )); then
				name="$(sed "$((id+1))q;d" $players_file | cut -f 2)"
				ask_confirmation "Set anchor rank for player $name as $rank?"
				comments="$(sed "$((id+1))q;d" $players_file | cut -f 7)"
				echo "Player #${id}: $name (${rating}±2*${sd})/0.06"
				temp_file=$(mktemp)
				head -n $id $players_file > $temp_file
				echo -e "$id\t$name\t${rank}\t${rating}.000\t${sd}.000\t0.06000\t$comments" >> $temp_file
				tail -n +$((id+2)) $players_file >> $temp_file
				mv $temp_file $players_file
				[ -z "$no_commit" ] && git add $players_file
				commit_msg="Asignación de grado ancla para $name ($rank)"
			else exit_err "Argument «$id» must be a valid player ID"; fi;;
		esac;;
	comment)
		check_digit $id
		n=$(cat $players_file | wc -l)
		if (( id > 0 )) && (( id < n )); then
			echo "Player #${id}: $(sed "$((id+1))q;d" $players_file | cut -f 2) ─ $comments"
			temp_file=$(mktemp)
			head -n $id $players_file > $temp_file
			echo -e "$(sed "$((id+1))q;d" $players_file | cut -f 1-6)\t$comments" >> $temp_file
			tail -n +$((id+2)) $players_file >> $temp_file
			mv $temp_file $players_file
			[ -z "$no_commit" ] && git add $players_file
			commit_msg="Comentarios para $(sed "$((id+1))q;d" $players_file | cut -f 2)"
		else exit_err "Argument «$id» must be a valid player ID"; fi;;
	delete)
		n=$(cat $matches_file | wc -l)
		(( n > 1 )) && exit_err "There cannot be any reported matches when deleting a player. Aborting."
		check_digit $id
		n=$(cat $players_file | wc -l)
		if (( id > 0 )) && (( id < n )); then
			name="$(sed "$((id+1))q;d" $players_file | cut -f 2)"
			ask_confirmation "Are you sure you want to delete player $name from the system?"
			temp_file=$(mktemp)
			head -n $id $players_file > $temp_file
			tail -n +$((id+2)) $players_file | while read line; do
				echo -e "$id\t$(echo -e "$line" | cut -f2-)"
				let id++
			done >> $temp_file
			mv $temp_file $players_file
			echo "Player #${id} $name deleted"
			[ -z "$no_commit" ] && git add $players_file
			commit_msg="Jugador $name eliminado del sistema"
		else exit_err "Argument «$id» must be a valid player ID"; fi;;
	report)
		check_digit $black
		check_digit $white
		n=$(cat $players_file | wc -l)
		if (( black > 0 )) && (( black < n )) && (( white > 0 )) && (( white < n )); then
			[ "$black" == "$white" ] && exit_err "Players arguments must differ"
			if [ "$result" == "0" ]; then result_text="lost against";
			elif [ "$result" == "1" ]; then result_text="won against";
			elif [ "$result" == "2" ]; then result_text="drew with";
			else exit_err "Argument «$result» is not a valid game result (must be either 0, 1 or 2)"; fi
			echo "Player #$black $result_text Player #$white"
			echo -e "$(date "+%-d/%-m/%Y %-H:%M:%S")\t$black\t$white\t$result" >> $matches_file
			exit 0
		else exit_err "Arguments «$black» and «$white» must be both valid player IDs"; fi;;
	empty)
		ask_confirmation "Are you sure you want to remove all reported matches?"
		temp_file=$(mktemp)
		head -n 1 $matches_file > $temp_file
		mv $temp_file $matches_file
		echo "Reported matches removed"
		exit 0
esac

echo -e "# $readme_title\n\n## $ratings_table_title\n" > README.md
temp_file=$(mktemp)
temp_tsv=$(mktemp)
tail -n+2 $players_file | while read line; do
	echo "$line" | awk 'BEGIN {FS="\t"}; {print $4"\t"$2"\t"$3"\t"$5"\t"$6"\t"$7}'
done | sort --general-numeric-sort --reverse | while read line; do
	echo "$line" | awk 'BEGIN {FS="\t"}; {print $2"\t"$3"\t"$1"\t"$4"\t"$5"\t"$6"\t"$7}'
done > $temp_file
head -n1 $players_file | sed "s/ID/Posición/" > $temp_tsv
ranking=1
while read line; do
	rank=$(echo "$line" | cut -f2)
	if [ "${rank:(-1)}" == "?" ]; then
		rank=-
	else
		rank=$ranking
		let ranking++
	fi
	echo -e "$rank\t$line" >> $temp_tsv
done < $temp_file
./tsv2html -m $temp_tsv >> README.md
rm -f $temp_file $temp_tsv
echo >> README.md
if [ -f $doc_file ]; then cat $doc_file >> README.md; fi
if [ -z "$no_commit" ]; then
	git add README.md
	git commit -m "$commit_msg" && [ -n "$remote" ] && git push $remote master
fi

